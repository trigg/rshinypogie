# fun
Functional Universal Nexus

This project's purpose is to demonstrate R shiny apps and github pages.

A plot of annual Gulf Menhaden (*Brevoortia patronus*) landings from this project is currently hosted at https://rtrigg.shinyapps.io/funtest/

Data is from NOAA Fisheries: https://www.st.nmfs.noaa.gov/commercial-fisheries/commercial-landings/annual-landings/index
